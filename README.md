# Digits Demo #

### This is a quick demo on how to get started with Digits powered by Twitter ###

* [Get Digits from here](http://https://get.fabric.io/digits)

### How do I get set up? ###

* Initialize a new Android Project
* Go to (https://get.fabric.io/) and download the Fabric Sdk for Android Studio
* Install the plug in by following the steps mentioned
* From the plugin, install Digits in your current project
* The plugin will make required changes to your Activity, Manifest and gradle files
* Do not forget to obfuscate your keys and secrets before launching 

### Who do I talk to? ###

* You can post your questions to (https://twittercommunity.com/) or write to Fabric for any queries.